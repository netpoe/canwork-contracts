pragma solidity ^0.4.24;

import './Service.sol';
import './Escrow.sol';
import './Dispute.sol';
import "./SafeMath.sol";

/**
* Questions
* what happens if the client raises a Dispute with another wallet. The Dispute gets resolved and the client gets part of the money back?
* how can we make sure that the client is no in the Dispute with another wallet?
* what % to give to a resolved Dispute?
* should the Dispute have a deadline? If the deadline is met, transfer the funds to the provider?
*/

contract CanWork {
    using SafeMath for uint256;

    string public version = '0.1';

    event CreatedService(address indexed client, address indexed Service);
    event CreatedEscrow(address indexed client, address indexed Escrow);
    event CreatedDispute(address indexed creator, address indexed Dispute);

    CanYaCoin public canyacoin;
    uint256 public payDividendsAfter;
    uint256 public collectDividendsBefore;
    uint256 public balance = 0;
    mapping (address => bool) public collectedDividends;
    event GotFee();

    struct Contracts {
        address Escrow;
        address Dispute;
    }

    Service[] public services;
    mapping (address => Service[]) public servicesByClient;
    mapping (address => Contracts) public serviceContracts;
  
    constructor() {
        canyacoin = 0x000...;
        payDividendsAfter = now + 3 months;
        collectDividendsBefore = payDividendsAfter + 3 days;
    }

    function () payable {
        createService();
    }

    /**
    * @dev Create a new Service contract
    * msg.sender is the client
    */
    function createService() public {
        client = msg.sender;
        Service service = new Service(client);
        servicesByClient[client].push(service);
        emit CreatedService(client, service);
    }

    /**
    * @dev Create a new Escrow contract and attach a Service contract to it
    * Record the Escrow into the serviceContracts index
    * Calculate and collect the fee from the msg.value
    * Deposit the reamaing ETH into the Escrow passing the ETH to the provider
    * @param Service contract
    */
    function createEscrow(Service service) public payable {
        require(service.provider() != address(0));
        Escrow escrow = new Escrow();
        serviceContracts[service].Escrow = escrow;
        emit CreatedEscrow(service.client(), escrow);
        uint256 fee = _collectFee();
        escrow.deposit(service.provider());
    }

    /**
    * @dev creates a Dispute contract and links it to a Service
    * should the Escrow funds be managed by the Dispute contract?
    * @param Service contract
    */
    function createDispute(Service service) public {
        require(serviceContracts[service].Dispute == address(0));
        require(msg.sender == service.client() || msg.sender == service.provider());
        service.dispute(msg.sender);
        Dispute dispute = new Dispute(service);
        serviceContracts[service].Dispute = dispute;
        emit CreatedDispute(msg.sender, dispute);
        escrow.transferPrimary(dispute);
    }

    /**
    * @dev pays the provider the Escrow funds
    */
    function payProvider(Service service) public {
        require(service.isFulfilled());
        Escrow escrow = serviceContracts[service].Escrow;
        escrow.withdraw(service.provider());
    }

    /**
    * @dev Get the 1% from the input amount
    * 1 ether * 0.01 = 1e16 WEI
    */
    function calculateFee(uint256 amount) public returns (uint256 fee) {
        uint256 fee = amount.mul(10000000000000000);
        return fee;
    }

    /**
    * @dev Calculates the CanYaCoin dividends corresponding to the msg.sender
    * Resets the calculateDividends mapping for the msg.sender
    * This allows the msg.sender to get dividends again
    */
    function calculateDividends() public returns (uint256) {
        return balance.mul(canyacoin.balanceOf(msg.sender)).div(canyacoin.totalSupply());
    }

    /**
    * @dev A CanYaCoin owner can call this function to get its ETH from the contract balance
    * Update the dates if collection period is over
    * Calculate and pay dividends if period is ongoing
    * Reset msg.sender ability to collect dividends otherwise, *the sender is responsible of remembering this
    */
    function collectDividends() public {
        if (now > collectDividendsBefore) {
            payDividendsAfter = collectDividendsBefore + 3 months;
            collectDividendsBefore = payDividendsAfter + 3 days;
        } else if (now >= payDividendsAfter && now <= collectDividendsBefore) {
            require(!collectedDividends[msg.sender]);
            uint256 dividends = calculateDividends();
            msg.sender.transfer(dividends);
            collectedDividends[msg.sender] = true;
        } else {
            collectedDividends[msg.sender] = false;
        }
    }

    /**
    * @dev collects the fee adding it to the CanWork public balance
    */
    function _collectFee() internal {
        uint256 amount = msg.value;
        uint256 fee = calculateFee(amount);
        balance = balance.add(fee);
    }
}