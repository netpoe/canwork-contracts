# CanWork Solidity contracts

CanWork is the decentralised serviceplace of the world. 

CanWork ecosystem consists of 4 main contracts:

- CanWork.sol
- Service.sol
- Escrow.sol
- Dispute.sol

## CanWork.sol

This contract creates Service, Escrow and Dispute contracts, serving as a index of the Service contracts.

CanWork contract also collects the 1% CanYa fee via the `balance` property. The fee is collected when an Escrow contract is created.

**CanYa Coin hodlers may collect their dividends 1 time every 3 months during a period of 3 days**.

## Service.sol

This contract handles the state of a service. The creator of this contract is the `client`.

A Service contract can have multiple `pendingProviders` and the client approves only 1 provider.

### Service states

```sol
enum State {
    pendingProvider,
    pendingCompletion,
    complete,
    cancelled,
    fullfilled,
    onDispute
}
```

#### pendingProvider

A 




# Development stages

1. Add Ethereum ropsten wallet functionality
1. Create a service, no title, no description
2. Add a title and description with dSchema and IPFS
3. Create an Escrow from existing Service contract
4. Display service states
5. Provider applies for service
6. Client approves a provider
7. Provider marks the service as complete
8. Client marks the service as fulfilled
9. Create a Service, an Escrow, approve a provider and
10. Provider creates a Dispute
11. Client creates a Dispute
12. CanWork contract transfers the Escrow to the Dispute contract
12. Wallets join an oingoing Dispute
13. Participants discuss on a Dispute
14. Participants vote for either the client or the provider
15. Dispute resolves the votes and delivers the funds to the winning party (client or the provider)
16. Dispute distributes the funds to the participants